const fs2 = require('fs');
const http = require('http');

//The child_process.exec() method runs a command in a console and buffers the output
const exec = require('child_process').exec;
const index = fs2.readFileSync(__dirname + '/index.html');

// Specify the File Path
const fileName = "logger.txt";

// Updating text in file
require('./writelogs').writeLogs();

const app = http.createServer((req, res)=> {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(index);
});

const io = require('socket.io').listen(app);

io.on('connection', (socket)=> { // contain new information
console.log('socket connected!');
    fs2.open(fileName, 'r', (err)=> { 
        // console.log(socket);

        exec("tail " + fileName, (err, tailLogs) => { //tailLogs is the content of file 
        //this will execute only once(1st time)
            tailLogs = tailLogs.replace(new RegExp('\n','g'), '<br />');  //replace --> \n to <br /> --> 'g' global replace all matches cases at once
            if(!tailLogs){
                tailLogs = "<h3>Log File Empty</h3>"; //display if file is empty
            }
        // emit throws the message to client or browser
            socket.emit('logsSync', { message: tailLogs });
        });

        //after every update in file 'watch' will triger
        fs2.watch(fileName, (event)=>{
// console.log(event);
            if(event === "change") {
                exec("tail " + fileName, (err, tailLogs) => {
                    tailLogs = tailLogs.replace(new RegExp('\n','g'), '<br />'); //replace --> \n to <br /> --> 'g' global replace all matches cases at once
                    if(!tailLogs){
                        tailLogs = "<h3>Log File Empty</h3>";
                    }
                socket.emit('logsSync', { message: tailLogs });
                });
            }
        });
    });
});

app.listen(8080);
